#-------------------------------------------------------------------------------
# Name:        Rubik's cube solver
#
# Author:      sprajagopal
#
# Created:     2014/03/07
# Copyright:   (c) sprajagopal 2014
#-------------------------------------------------------------------------------

import scipy as sp
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import sympy as syp
import pdb
import time
pdb.set_trace()


# time measurements
# START time
time_progstart = time.time()

# GLOBAL VARIABLES
ARG_SCRM_NOS  = 7
PEN_XNEG_BIAS = 1
ES_POP_CFRAC  = 0.9
MAX_RECURS    = 10

# CORE evolution strategy for the optimization
# define ES parameters
glob_lambda   = 1000
glob_genmax   = 30
glob_popsize  = 100
glob_stagtol  = 3
glob_selstrat = 'mupluslambda'


# 1. ROTATION MATRICES
rotmats_x = np.array([
    [1,0,0],
    [0,0,-1],
    [0,1,0]])

rotmats_y = np.array([
    [0,0,1],
    [0,1,0],
    [-1,0,0]])

rotmats_z = np.array([
    [0,-1,0],
    [1,0,0],
    [0,0,1]])
rotmats = [rotmats_x,rotmats_y,rotmats_z]


# 2. COLOUR -> NUMBERS
cube_colours  = np.array([1,2,3,4,5,6])
[b,w,h,y,g,r] = cube_colours[:]

## 2. COlOURS-NUMBERS
#h = syp.Symbol('h')
#w = syp.Symbol('w')
#r = syp.Symbol('r')
#g = syp.Symbol('g')
#b = syp.Symbol('b')
#y = syp.Symbol('y')

# 3. Reference AXES-COLOURS info
fref_xpos = np.array([r,0,0]);
fref_xneg = np.array([-h,0,0]);
fref_ypos = np.array([0,w,0]);
fref_yneg = np.array([0,-y,0]);
fref_zpos = np.array([0,0,b]);
fref_zneg = np.array([0,0,-g]);

# 4. PENALTY factors
PENFAC_FACELETS = 1
PENFAC_EDGES    = 4
PENFAC_CORNERS  = 6

# 5. EDGE AND CORNER PENALTY reference vectors
corner_refvecs = np.empty([3,3,3], dtype = list)
edge_refvecs   = np.empty([3,3,3], dtype = list)

#------- CORNERS
corner_refvecs[2,2,2] = [r,w,b]
corner_refvecs[2,2,0] = [r,w,-g]
corner_refvecs[2,0,0] = [r,-y,-g]
corner_refvecs[2,0,2] = [r,-y,b]

corner_refvecs[0,2,2] = [-h,w,b]
corner_refvecs[0,0,2] = [-h,-y,b]
corner_refvecs[0,0,0] = [-h,-y,-g]
corner_refvecs[0,2,0] = [-h,w,-g]

#------- EDGES
edge_refvecs[2,1,2] = [r,0,b]
edge_refvecs[2,2,1] = [r,w,0]
edge_refvecs[2,1,0] = [r,-g,0]
edge_refvecs[2,0,1] = [r,-y,0]

edge_refvecs[1,0,2] = [0,-y,b]
edge_refvecs[1,2,2] = [0,w,b]
edge_refvecs[1,2,0] = [0,w,-g]
edge_refvecs[1,0,0] = [0,-y,-g]

edge_refvecs[0,1,2] = [-h,0,b]
edge_refvecs[0,2,1] = [-h,w,0]
edge_refvecs[0,1,0] = [-h,0,-g]
edge_refvecs[0,0,1] = [-h,-y,0]

# 6. Mutation moves in Local Coordinate of faces
# face_localmoves = ( F,B,R,L,U,D,
                    # Fi,Bi,Ri,Li,Ui,Di )



#------------ AXIS based cw ccw rotations

Xp = '033'
Xn = '011'
Yp = '133'
Yn = '111'
Zp = '233'
Zn = '211'

Xpi = '031'
Xni = '013'
Ypi = '131'
Yni = '113'
Zpi = '231'
Zni = '213'

Xm = '023'
Ym = '123'
Zm = '223'

Xmi = '021'
Ymi = '121'
Zmi = '221'

#------------- Local meanings of f,b,r,l,u,d
#------------- Included middle slice moves
xpos_lm = [ Xp,Xn,Yp,Yn,Zp,Zn,
            Xpi,Xni,Ypi,Yni,Zpi,Zni,
            Xm,Ym,Zm,
            Xmi,Ymi,Zmi]

xneg_lm = [ Xn,Xp,Yn,Yp,Zp,Zn,
            Xni,Xpi,Yni,Ypi,Zpi,Zni,
            Xmi,Ymi,Zm,
            Xm,Ym,Zmi]

ypos_lm = [ Yp,Yn,Xn,Xp,Zp,Zn,
            Ypi,Yni,Xni,Xpi,Zpi,Zni,
            Ym,Xmi,Zm,
            Ymi,Xm,Zmi]

yneg_lm = [ Yn,Yp,Xp,Xn,Zp,Zn,
            Yni,Ypi,Xpi,Xni,Zpi,Zni,
            Ymi,Xm,Zm,
            Ym,Xmi,Zmi]

zpos_lm = [ Zp,Zn,Yn,Yp,Xn,Xp,
            Zpi,Zni,Yni,Ypi,Xni,Xpi,
            Zm,Ym,Xmi,
            Zmi,Ymi,Xm]

zneg_lm = [ Zn,Zp,Yp,Yn,Xn,Xp,
            Zni,Zpi,Ypi,Yni,Xni,Xpi,
            Zmi,Ymi,Xmi,
            Zm,Ym,Xm]


#------------ LIST OF MUTATE MOVES
#------------ HERDY ES Mutation operators from elsourani2010
#------------ mutations = list of moves = list of strings

# --- assigning dummy values
[front,back,right,left,up,down,
 frontinv,backinv,rightinv,leftinv,upinv,downinv,
 facemid,hormid,vertmid,facemidinv,hormidinv,vertmidinv] =  xpos_lm[:]

moves_list = []


## two edge flip cw
moves_list.append([front,right,back,left,up,leftinv,up,backinv,rightinv,frontinv,leftinv,upinv,left,upinv])

## two edge flip ccw
moves_list.append([frontinv,leftinv,backinv,rightinv,upinv,right,upinv,back,left,front,right,up,rightinv,up])

## two corner flip cw
moves_list.append([left,downinv,leftinv,frontinv,downinv,front,up,frontinv,down,front,left,down,leftinv,upinv])

## two corner flip ccw
moves_list.append([rightinv,down,right,front,down,frontinv,upinv,front,downinv,frontinv,rightinv,downinv,right,up])

## three edge swap cw
moves_list.append([up,front,front,upinv,rightinv,downinv,leftinv,front,front,left,down,right])

## three edge swap ccw
moves_list.append([upinv,front,front,up,left,down,right,front,front,rightinv,downinv,leftinv])

## two edge/corner swap cw
moves_list.append([rightinv,up,right,upinv,rightinv,up,front,right,backinv,right,back,right,frontinv,right,right])

## two edge/corner swap ccw
moves_list.append([left,upinv,leftinv,up,left,upinv,frontinv,leftinv,back,leftinv,backinv,leftinv,front,left,left])

## three corner swap cw
moves_list.append([frontinv,up,back,upinv,front,up,backinv,upinv])

## three corner swap ccw
moves_list.append([front,upinv,backinv,up,frontinv,upinv,back,up])

## three inslice edge swap cw
moves_list.append([right,leftinv,up,up,rightinv,left,front,front])

## three inslice edge swap ccw
moves_list.append([leftinv,right,up,up,left,rightinv,front,front])

## single moves
## only used at stagnations
moves_list.append([leftinv])
moves_list.append([rightinv])
moves_list.append([left])
moves_list.append([right])
moves_list.append([up])
moves_list.append([down])
moves_list.append([upinv])
moves_list.append([downinv])
moves_list.append([backinv])
moves_list.append([frontinv])
moves_list.append([back])
moves_list.append([front])
moves_list.append([facemid])
moves_list.append([vertmid])
moves_list.append([hormid])

moves_smp_STARTIND = 12
moves_smp_ENDIND   = len(moves_list) - 1

# penalty modules

def cube_pen_facelets(cube_state):
    # PENALTY Q1 - for facelets in each face
    # SORT VECTORS into separate FACES

    pen_facelets = 0
    # 1. X-axis
    face_xpos = cube_state[cube_state[:,0] > 0]
    face_xneg = cube_state[cube_state[:,0] < 0]

    # 2. Y-axis
    face_ypos = cube_state[cube_state[:,1] > 0]
    face_yneg = cube_state[cube_state[:,1] < 0]

    # 3. Z-axis
    face_zpos = cube_state[cube_state[:,2] > 0]
    face_zneg = cube_state[cube_state[:,2] < 0]

    # ASSIGN PENALTY for facelets
    face_xpos_diffvec = face_xpos[:,0] - fref_xpos[0]
    pen_facelets = pen_facelets + len(face_xpos_diffvec) - len(face_xpos_diffvec[face_xpos_diffvec == 0])

    face_xneg_diffvec = face_xneg[:,0] - fref_xneg[0]
    pen_facelets = pen_facelets + PEN_XNEG_BIAS*(len(face_xneg_diffvec) - len(face_xneg_diffvec[face_xneg_diffvec == 0]))

    face_ypos_diffvec = face_ypos[:,1] - fref_ypos[1]
    pen_facelets = pen_facelets + len(face_ypos_diffvec) - len(face_ypos_diffvec[face_ypos_diffvec == 0])

    face_yneg_diffvec = face_yneg[:,1] - fref_yneg[1]
    pen_facelets = pen_facelets + len(face_yneg_diffvec) - len(face_yneg_diffvec[face_yneg_diffvec == 0])

    face_zpos_diffvec = face_zpos[:,2] - fref_zpos[2]
    pen_facelets = pen_facelets + len(face_zpos_diffvec) - len(face_zpos_diffvec[face_zpos_diffvec == 0])

    face_zneg_diffvec = face_zneg[:,2] - fref_zneg[2]
    pen_facelets = pen_facelets + len(face_zneg_diffvec) - len(face_zneg_diffvec[face_zneg_diffvec == 0])

    # multiply by FACELET FACTOR
    pen_facelets = pen_facelets*PENFAC_FACELETS

    return pen_facelets

# Zero module - single face cube fitness
def cube_vecfitness(cube_state):

    # 1. X-axis
    face_xpos = cube_state[cube_state[:,0] > 0]
    face_xneg = cube_state[cube_state[:,0] < 0]

    # 2. Y-axis
    face_ypos = cube_state[cube_state[:,1] > 0]
    face_yneg = cube_state[cube_state[:,1] < 0]

    # 3. Z-axis
    face_zpos = cube_state[cube_state[:,2] > 0]
    face_zneg = cube_state[cube_state[:,2] < 0]

    pdb.set_trace()

    return 0


# zero module - calculating fitness
def cube_fitness(cube_state):

    pen_edges = pen_corners = 0

    # PENALTY FOR FACELETS
    pen_facelets = cube_pen_facelets(cube_state)

    # FILTERING EDGE AND CORNERS
    cube_bool_zeroelems   = (cube_state == 0)
    cube_no_zeroelems_row = np.sum(cube_bool_zeroelems,1)
    cube_edges            = cube_state[cube_no_zeroelems_row == 1]
    cube_corners          = cube_state[cube_no_zeroelems_row == 0]

   # print "\ncube_edges\n"
   # print cube_edges
   # print "\ncube_corners\n"
   # print cube_corners

    # PENALTY Q2 - for EDGES
    # TODO
    edge_indices = np.sign(cube_edges) + 1

    # edge position error
    edge_vecs_indiced = np.array(
        np.array(
            edge_refvecs[
                (edge_indices[:,0]).tolist(),
                (edge_indices[:,1]).tolist(),
                (edge_indices[:,2]).tolist()
            ]).tolist())


    edge_poserrs = np.apply_along_axis(sorted,axis = 1,arr = cube_edges) - np.apply_along_axis(sorted,axis = 1,arr = edge_vecs_indiced)

    # MAGNITUBE of position errors
    edge_poserrs_mag = np.apply_along_axis(np.linalg.norm, axis = 1, arr = edge_poserrs)
    # number of violations and corres. penalty
    edge_noviols = len(edge_poserrs_mag) - len(edge_poserrs[edge_poserrs_mag == 0])
    pen_edges = edge_noviols*PENFAC_EDGES
    # END OF PENLATY Q2



    # PENALTY Q3 - for CORNERS

    # SIGNS to RefVec indices
    corner_indices = np.sign(cube_corners) + 1

    # Calculate CORNER pos error
    corner_vecs_indiced = np.array(
        np.array(
            corner_refvecs[
                (corner_indices[:,0]).tolist(),
                (corner_indices[:,1]).tolist(),
                (corner_indices[:,2]).tolist()
            ]).tolist())


    corner_poserrs = np.apply_along_axis(sorted,axis = 1,arr = cube_corners) - np.apply_along_axis(sorted,axis = 1,arr = corner_vecs_indiced)

    # MAGNITUDE of corner position error
    corner_poserrs_mag = np.apply_along_axis(np.linalg.norm, axis = 1, arr = corner_poserrs)
    # number of violations and corres. penalty
    corner_noviols = len(corner_poserrs_mag) - len(corner_poserrs[corner_poserrs_mag == 0])
    pen_corners = corner_noviols*PENFAC_CORNERS
    # END OF PENALTY Q3


    return pen_facelets + pen_edges + pen_corners
    # END OF PENALTY MODULE, FITNESS MODULE

# first module  - representing cube and simulating movements
def cube_move(cube_state, move):

    # print integrity before move
    # print "---------------------------------"
    # print move, sum(cube_state[:,0] == 0),sum(cube_state[:,1] == 0),sum(cube_state[:,2] == 0)

    # parse move = axis, slice, angle
    move_axis = int(move[0])
    move_slice = int(move[1])
    move_angle = int(move[2])

    # SORT according to AXIS
    # 0 = X
    # 1 = Y
    # 2 = Z
    cube_state = cube_state[cube_state[:,move_axis].argsort()]

    # SLICE
    # 1 = -ve face
    # 2 = 0 face (middle slice)
    # 3 = +ve face

    # CHUNK according to SLICE

    if(move_slice == 2):
        if(move[2] == '1'):
            cube_state = cube_move(cube_state,
                               move[0] + '3' + '3')
            cube_state = cube_move(cube_state,
                               move[0] + '1' + '3')
        elif(move[2] == '3'):
            cube_state = cube_move(cube_state,
                               move[0] + '3' + '1')
            cube_state = cube_move(cube_state,
                               move[0] + '1' + '1')
        else:
            cube_state = cube_move(cube_state,
                               move[0] + '3' + '2')
            cube_state = cube_move(cube_state,
                               move[0] + '1' + '2')
        return cube_state

    else:

        slice_inds = np.array( [[0,9],[9,17],[17,27]] )
        cube_slice_old = cube_state[
            slice_inds[move_slice-1,0]:slice_inds[move_slice-1,1],
            :]



    #    print "\nCube slice to be moved\t:",cube_slice_old,"\n"
        # MULTIPLY according to angle
        # power function for multiple rotations
        # move_angle:
        # 1 = ONE QUARTER
        # 2 = TWO QUARTERS, etc.
        move_rotate_val = np.linalg.matrix_power(rotmats[move_axis],move_angle)
        cube_slice_new = np.dot(move_rotate_val,
                                cube_slice_old.T)
        # print "===",cube_state.T,"\n\n"
        # print cube_slice_new,"\n\n\n",move_rotate_val,"\n",cube_slice_old.T
        # replace cube_state with new slice
        cube_state[
        slice_inds[move_slice-1,0]:slice_inds[move_slice-1,1],
        :] = cube_slice_new.T

        # print move, sum(cube_state[:,0] == 0),sum(cube_state[:,1] == 0),sum(cube_state[:,2] == 0)
        return cube_state



# second module - tracking system, a output in 2d form of cube state
def cube_print(cube_state,filename='temp.pdf'):

    # TRIPLE SORT each face
    # 1. X face sort; column 1 sort
    cube_state = cube_state[cube_state[:,0].argsort()]

    # 1. Xpos. Col 2 sort
    cube_state[0:9] = cube_state[cube_state[0:9,1].argsort()]
    # 2. X0. Col 2 sort
    cube_state[9:17] = cube_state[9 + cube_state[9:17,1].argsort()]
    # 3. Xneg. Col 2 sort
    cube_state[17:26] = cube_state[17 + cube_state[17:26,1].argsort()]


    # 1.1. Xpos Y sort
    cube_state[0:3] = cube_state[cube_state[0:3,2].argsort()]
    cube_state[3:6] = cube_state[3 + cube_state[3:6,2].argsort()]
    cube_state[6:9] = cube_state[6 + cube_state[6:9,2].argsort()]


    # 2.1 X0 Y sort
    cube_state[9:12] = cube_state[9 + cube_state[9:12,2].argsort()]
    cube_state[12:14] = cube_state[12 + cube_state[12:14,2].argsort()]
    cube_state[14:17] = cube_state[14 + cube_state[14:17,2].argsort()]


    # 3.1 Xneg Y sort
    cube_state[17:20] = cube_state[17 + cube_state[17:20,2].argsort()]
    cube_state[20:23] = cube_state[20 + cube_state[20:23,2].argsort()]
    cube_state[23:26] = cube_state[23 + cube_state[23:26,2].argsort()]

    # 4. Store in nd array as per template
    cs = np.array([[0,0,0]])
    cs = np.concatenate((cs,cube_state),axis = 0) # alias

    print_cube = np.zeros((12,9))

    print_cube[0,3:6] = [
        cs[18,2],
        cs[21,2],
        cs[24,2]
        ][:]
    print_cube[1,3:6] = [
        cs[10,2],
        cs[13,2],
        cs[15,2]
        ][:]
    print_cube[2,3:6] = [
        cs[1,2],
        cs[4,2],
        cs[7,2]
        ][:]

    print_cube[3,0:9] = [
        cs[18,1],
        cs[10,1],
        cs[1,1],
        cs[1,0],
        cs[4,0],
        cs[7,0],
        cs[7,1],
        cs[15,1],
        cs[24,1]
][:]
    print_cube[4,0:9] = [
        cs[19,1],
        cs[11,1],
        cs[2,1],
        cs[2,0],
        cs[5,0],
        cs[8,0],
        cs[8,1],
        cs[16,1],
        cs[25,1],
    ][:]
    print_cube[5,0:9] = [
        cs[20,1],
        cs[12,1],
        cs[3,1],
        cs[3,0],
        cs[6,0],
        cs[9,0],
        cs[9,1],
        cs[17,1],
        cs[26,1],
    ][:]

    print_cube[6,3:6] = [
        cs[3,2],
        cs[6,2],
        cs[9,2],
][:]
    print_cube[7,3:6] = [
        cs[12,2],
        cs[14,2],
        cs[17,2],
][:]
    print_cube[8,3:6] = [
        cs[20,2],
        cs[23,2],
        cs[26,2],
][:]

    print_cube[9,3:6] = [
        cs[20,0],
        cs[23,0],
        cs[26,0],
][:]
    print_cube[10,3:6] = [
        cs[19,0],
        cs[22,0],
        cs[25,0],
][:]
    print_cube[11,3:6] = [
        cs[18,0],
        cs[21,0],
        cs[24,0],
][:]

    print_cube = np.abs(print_cube)
    #print print_cube

    # plotting the colours
    plot_fig = plt.figure()

    line_1 = plt.plot(np.where(print_cube == 1)[0],np.where(print_cube == 1)[1])
    line_2 = plt.plot(np.where(print_cube == 2)[0],np.where(print_cube == 2)[1])
    line_3 = plt.plot(np.where(print_cube == 3)[0],np.where(print_cube == 3)[1])
    line_4 = plt.plot(np.where(print_cube == 4)[0],np.where(print_cube == 4)[1])
    line_5 = plt.plot(np.where(print_cube == 5)[0],np.where(print_cube == 5)[1])
    line_6 = plt.plot(np.where(print_cube == 6)[0],np.where(print_cube == 6)[1])

    plot_ms = 20.0
    plot_mfc = ['blue','pink','black','yellow','green','red'][:]
    plot_marker = 's'

    plt.setp(line_1,'mfc',plot_mfc[0],'ms',plot_ms,'linestyle','None','marker',plot_marker)
    plt.setp(line_2,'mfc',plot_mfc[1],'ms',plot_ms,'linestyle','None','marker',plot_marker)
    plt.setp(line_3,'mfc',plot_mfc[2],'ms',plot_ms,'linestyle','None','marker',plot_marker)
    plt.setp(line_4,'mfc',plot_mfc[3],'ms',plot_ms,'linestyle','None','marker',plot_marker)
    plt.setp(line_5,'mfc',plot_mfc[4],'ms',plot_ms,'linestyle','None','marker',plot_marker)
    plt.setp(line_6,'mfc',plot_mfc[5],'ms',plot_ms,'linestyle','None','marker',plot_marker)

    plt.axis([-2,14,-2,10])

    ax = plot_fig.add_subplot(111)
    ax.text(10,8,str(cube_fitness(cube_state)))
    ax.text(10,6,'Xpos')
    ax.text(0,2,'Zneg')
    ax.text(4,9,'Ypos')
    ax.text(4,-1.5,'Yneg')
    ax.text(6,2,'Zpos')

# figure save place
    print_dir = './cube_states/'
    print_figname = filename
    plt.savefig(print_dir + print_figname)
    plt.close(plot_fig)
    return 0

#def cube_init(cube_syms_state):
    # initialize a random cube configuration
##    cube_state = np.array([
# [h,-w,b],
# [h,0,w],
# [y,h,g],
# [y,h,0],
# [w,r,-b],
# [r,0,-w],
# [g,-r,-y],
# [h,-g,0],
# [w,0,0],
# [-y,b,r],
# [-r,g,0],
# [-y,b,-h],
# [-b,0,r],
# [-y,0,0],
# [-b,0,-w],
# [-w,-g,h],
# [-r,-y,0],
# [-r,-w,-g],
# [0,-h,b],
# [0,-r,0],
# [0,-w,-g],
# [0,0,w],
# [0,0,-g],
# [0,y,g],
# [0,h,0],
# [0,b,-y]])

#    cube_state = cube_syms_state
#    return cube_state

def cube_example_conf():

    cube_state = np.array([
    [r,0,0],
    [h,0,g],
    [b,r,w],
    [g,y,0],
    [w,g,-r],
    [w,0,-h],
    [h,-w,-g],
    [g,-w,0],
    [b,-h,w],

    [-y,0,r],
    [-h,0,0],
    [-g,-y,h],
    [-w,-b,0],
    [-y,-r,-g],
    [-h,0,-y],
    [-b,h,-y],
    [-b,h,0],
    [-r,y,b],

    [0,-y,0],
    [0,0,b],
    [0,y,b],
    [0,w,0],
    [0,g,-r],
    [0,0,-g],
    [0,-b,-r],
    [0,-w,r]])

    return cube_state

def cube_mutate(cube_state,flag_stagnation,mut_num_moves):
    # TODO: mutation moves list. HARDCODING

    # PROB. FACE selection
    # OPTIONS: xpos_lm, xneg_lm, ypos_lm, yneg_lm, zpos_lm, zneg_lm
    for i_move in range(0,mut_num_moves,1):
        opt_move_faces = [xpos_lm,xneg_lm,
                          ypos_lm,yneg_lm,
                          zpos_lm,zneg_lm]

        ps_move_face = np.random.randint(0,6,size = 1).tolist()[0]


        #--- assigning values for local directions
        #--- according to face selected above

        [front,back,right,left,up,down,
         frontinv,backinv,rightinv,leftinv,upinv,downinv,
         facemid,hormid,vertmid,facemidinv,hormidinv,vertmidinv] =  opt_move_faces[ps_move_face][:]

        # PROB. MOVE selection
        # 0 -> len(moves_list) for indice from moves_list

        # if best fitness is stagnant for last ES_STAG_TOL generations
        if flag_stagnation == 1:
            #ps_move_combo = np.random.randint(moves_smp_STARTIND,
            #                                  moves_smp_ENDIND + 1,
            #                                  size = 1).tolist()[0]

            ps_move_combo = np.random.randint(0,
                                              moves_smp_STARTIND,
                                              size = 1).tolist()[0]
        # else perform normal combo mutation
        elif flag_stagnation == 0:
            ps_move_combo = np.random.randint(0,
                                              len(moves_list),
#moves_smp_STARTIND,
                                              size = 1).tolist()[0]

        else:
            ps_move_combo = np.random.randint(moves_smp_STARTIND,
                                              moves_smp_ENDIND + 1,
                                              size = 1).tolist()[0]

            # MOVES on the cube
        for curr_move in moves_list[ps_move_combo]:
            cube_state = cube_move(cube_state,curr_move)


    return cube_state

def es_herdy(cube_state,
             ES_LAMBDA = glob_lambda,
             ES_GEN_MAX = glob_genmax,
             ES_POP_SIZE = glob_popsize,
             ES_STAG_TOL = glob_stagtol,
             es_sel_strat = glob_selstrat):


    mut_num_moves   = 1
    flag_stagnation = 0
    flag_recur      = 0
    #es_sel_strat    = 'parchild'

    # 1. INITIALIZE population
    # ----- mutate unsolved cube to ES_POP_SIZE different variations
    es_pop = [cube_state]
    for i_initpop in range(0,ES_POP_SIZE - 1,1):
        es_pop.append(cube_mutate(cube_state,flag_stagnation,mut_num_moves))

    #### START LOOP of generations.
    # ---- ES_GEN_MAX
    es_gen_avgfit = []
    es_gen_bestfit = []
    es_gen_bestind = []

    for curr_gen in range(0,ES_GEN_MAX,1):

        # check STAGNATION

        if (curr_gen != 0) & (curr_gen > ES_STAG_TOL):
            if (all(es_gen_bestfit[curr_gen - 1 ] == es_gen_bfs_prev for es_gen_bfs_prev in
                                                       es_gen_bestfit[ curr_gen - ES_STAG_TOL : curr_gen])):

                flag_recur = 1
                return es_gen_bestfit,es_gen_avgfit,es_pop,es_gen_bestind,flag_recur
                #flag_stagnation = 1
                #mut_num_moves = mut_num_moves + 1

            else:
                flag_stagnation = 0
        # SPAWN ES_LAMBDA children from ES_MU parents
        es_children = []
        es_child_parinds = np.random.randint(0,ES_POP_SIZE,size = ES_LAMBDA)
        for i_parind in es_child_parinds:
            es_children.append(cube_mutate(
                                   es_pop[
                                   i_parind],flag_stagnation,mut_num_moves))


         # SELECTION SCHEME 1: mu plus lambda
         # FITNESS for ES_SELPOOL = CHILDREN + PARENTS
        if es_sel_strat == 'mupluslambda':
             es_selpool = es_pop + es_children

             # key values to later select inds
             es_selpool_fitness = [range(0,ES_POP_SIZE + ES_LAMBDA,1)]
             es_selpool_fvals = []


             for i_currcube in es_selpool:
                 es_selpool_fvals.append(cube_fitness(
                     i_currcube))


             # print best inds every iteration
             es_selpool_fitness.append(es_selpool_fvals)

             # SELECTION of ES_MU best individuals
             # this is ES_POP
             es_selpool_fitness = np.array(es_selpool_fitness).T
             es_selpool_fitness = es_selpool_fitness[es_selpool_fitness[:,1].argsort()]
             es_selpool_nextinds = np.array(es_selpool_fitness[0:ES_POP_SIZE])[:,0]
             es_pop = [ es_selpool[i] for i in es_selpool_nextinds.tolist()]

             es_pop_fvals = []
             for i_currcube in es_pop:
                 es_pop_fvals.append(cube_fitness(
                     i_currcube))

             es_gen_avgfit.append(np.mean(es_pop_fvals))
             es_gen_bestfit.append(np.min(es_pop_fvals))
             es_gen_bestind.append(( es_pop[np.argmin(es_pop_fvals)]))
             cube_print(es_gen_bestind[len(es_gen_bestind) - 1])

        # SELECTION SCHEME 2: some from parents, some from children
        if es_sel_strat == 'parchild':

            ES_SEL_CHILD = int(ES_POP_CFRAC*ES_POP_SIZE)
            ES_SEL_PAR = ES_POP_SIZE - ES_SEL_CHILD
            print ES_SEL_CHILD,ES_SEL_PAR
            # from children
            es_selpool = es_children

            #key values to later select inds
            es_selpool_fitness = [range(0,ES_LAMBDA,1)]
            es_selpool_fvals = []

            for i_currcube in es_selpool:
                es_selpool_fvals.append(cube_fitness(
                    i_currcube))

            es_selpool_fitness.append(es_selpool_fvals)

            es_selpool_fitness = np.array(es_selpool_fitness).T
            es_selpool_fitness = es_selpool_fitness[es_selpool_fitness[:,1].argsort()]
            es_selpool_nextinds = np.array(es_selpool_fitness[0:ES_SEL_CHILD])[:,0]
            es_pop_child = [ es_selpool[i] for i in es_selpool_nextinds.tolist()]

            # from parents
            es_selpool = es_pop

            # key values to later select inds
            es_selpool_fitness = [range(0,ES_POP_SIZE,1)]
            es_selpool_fvals = []
            for i_currcube in es_selpool:
                es_selpool_fvals.append(cube_fitness(
                    i_currcube))

            es_selpool_fitness.append(es_selpool_fvals)


            es_selpool_fitness = np.array(es_selpool_fitness).T
            es_selpool_fitness = es_selpool_fitness[es_selpool_fitness[:,1].argsort()]
            es_selpool_nextinds = np.array(es_selpool_fitness[0:ES_SEL_PAR])[:,0]
            es_pop_par = [ es_selpool[i] for i in es_selpool_nextinds.tolist()]

            es_pop = es_pop_child + es_pop_par
            es_pop_fvals = []
            for i_currcube in es_pop:
                es_pop_fvals.append(cube_fitness(
                    i_currcube))

            es_gen_avgfit.append(np.mean(es_pop_fvals))
            es_gen_bestfit.append(np.min(es_pop_fvals))
            es_gen_bestind.append(( es_pop[np.argmin(es_pop_fvals)]))
            cube_print(es_gen_bestind[len(es_gen_bestind) - 1])
    #### END LOOP
    flag_recur = 0
    return es_gen_bestfit,es_gen_avgfit,es_pop,es_gen_bestind,flag_recur

def cube_scrambler(SCRM_NOS):

    # solved cube conf
    cube_state = np.array([
        [r,0,0],
        [r,0,b],
        [r,w,b],
        [r,w,0],
        [r,w,-g],
        [r,0,-g],
        [r,-y,-g],
        [r,-y,0],
        [r,-y,b],

        [-h,0,b],
        [-h,0,0],
        [-h,-y,b],
        [-h,-y,0],
        [-h,-y,-g],
        [-h,0,-g],
        [-h,w,-g],
        [-h,w,0],
        [-h,w,b],

        [0,-y,0],
        [0,0,b],
        [0,w,b],
        [0,w,0],
        [0,w,-g],
        [0,0,-g],
        [0,-y,-g],
        [0,-y,b]])

    # perform SCRM_NOS scrambles

    # PROB. FACE selection
    # OPTIONS: xpos_lm, xneg_lm, ypos_lm, yneg_lm, zpos_lm, zneg_lm
    opt_move_faces = [xpos_lm,xneg_lm,
                      ypos_lm,yneg_lm,
                      zpos_lm,zneg_lm]

    ps_move_face = np.random.randint(0,6,size = SCRM_NOS).tolist()




    # PROB. MOVE selection
    # 0 -> len(moves_list) for indice from moves_list

    # if best fitness is stagnant for last ES_STAG_TOL generations
    ps_move = np.random.randint(moves_smp_STARTIND,
                                          moves_smp_ENDIND + 1,
                                          size = SCRM_NOS).tolist()
    for i_move in range(0,SCRM_NOS,1):
        #--- assigning values for local directions
        #--- according to face selected above

        [front,back,right,left,up,down,
         frontinv,backinv,rightinv,leftinv,upinv,downinv,
         facemid,hormid,vertmid,facemidinv,hormidinv,vertmidinv] =  opt_move_faces[ps_move_face[i_move]][:]
        cube_state = cube_move(cube_state,moves_list[ps_move
                                                     [i_move
                                                          ]][0])

    return cube_state

def pop_print(pop):


    # Loop to print each ind in pop
    for curr_cube,i_filename in zip(pop,range(0,len(pop),1)):
        cube_print(curr_cube,str(i_filename) + '.png')
    return 0

def cube_sandbox(cube_state):

    pdb.set_trace()

    return 0
def main():

    cube_state = cube_scrambler(ARG_SCRM_NOS)
    #cube_state = cube_example_conf()
    print cube_state

    recur = 1
    i_recur = 1
    while (recur == 1) & (i_recur < MAX_RECURS) :
        [bestfit,avgfit,pop_final,bestind,recur] = es_herdy(cube_state)
        cube_state = bestind[ len(bestind) - 1 ]
        i_recur = i_recur + 1

    pop_print(pop_final)
    print "\nAverage fitness\tBest fitness\n"
    print np.array(map(int,avgfit)).T,np.array(bestfit).T

    # END TIME
    time_progend = time.time()
    print "\nExec. time:\t",time_progend - time_progstart,"\tseconds\n"

    #cube_print(bestind[len(bestind) - 1])
    # go to debug after exec
    cube_sandbox(bestind[len(bestind) - 1])
    return 0


def res_mupluslambda():
    results_array = []
    mpl_gensize = 50
    for i_scrms in range(1,5,1):
        cube_state = cube_scrambler(i_scrms)

        for i_popsize in range(5,10,5):
            [bestfit,avgfit,pop_final,bestind] = es_herdy(
                cube_state,
                5*i_popsize,
                mpl_gensize,
                i_popsize,
                5,
                'mupluslambda')
            print [i_scrms,
                   i_popsize,
                   avgfit[mpl_gensize - 1],
                   bestfit[mpl_gensize - 1]]
            results_array.append([i_scrms,
                                                i_popsize,
                                                avgfit[mpl_gensize - 1],
                                                bestfit[mpl_gensize - 1]])

            print results_array

    res_file = 'mupluslambda.txt'
    res_fhandle = open(res_file,"w")
    np.savetxt(res_fhandle,
                          results_array,
                          fmt='%3.3d',
                          delimiter=' & ',
                          newline='\\\\\n',
                          )
    return 0


main()
#res_mupluslambda()
#cube_sandbox(cube_example_conf())
